<?php

namespace App\Http\Controllers;

use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Throwable;


class UserController extends Controller
{
    public function index()
    {
        return view('index', [
            'users' => User::all()
        ]);
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'file' => 'nullbale|mimes:pdf,doc,docx|max:2048',
                'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
                'address' => 'required|string',
                'email' => 'required|string|email',
                'phone' => 'required|string|min:10|max:10',
                'gender' => 'required|integer|in:1,2',
                'name' => 'required|string',
            ]);
            $imageName = null;
            $fileName = null;
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images'), $imageName);
            }
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $fileName = time() . '.' . $file->getClientOriginalExtension();
                $file->move(public_path('files'), $fileName);
            }

            return DB::transaction(function () use ($request, $imageName, $fileName) {
                if (
                    $userAddress = User::find($request->input('id'))
                ) {
                    $message = 'User updated successfully';
                    $userAddress->name = $request->get('name');
                    $userAddress->phone = $request->get('phone');
                    $userAddress->address = $request->get('address');
                    $userAddress->gender = $request->get('gender');
                    $userAddress->email = $request->get('email');
                    $userAddress->image_path = isset($imageName) ? 'images/' . $imageName : null;
                    $userAddress->file_path = isset($fileName) ? 'files/' . $fileName : null;
                    $userAddress->save();
                } else {
                    $message = 'User add successfully';
                    User::create([
                        'name' => $request->get('name'),
                        'address' => $request->get('address'),
                        'gender' => $request->get('gender'),
                        'email' => $request->get('email'),
                        'phone' => $request->get('phone'),
                        'image_path' => isset($imageName) ? 'images/' . $imageName : null,
                        'file_path' => isset($fileName) ? 'files/' . $fileName : null,
                    ]);
                }
                return response()->json(['status' => true, 'message' => $message, 'users' => User::all()]);
            });

        } catch (ValidationException $e) {
            return response()->json(['status' => false, 'errors' => $e->validator->getMessageBag()], 422);
        } catch (\Throwable $e) {
            return response()->json(['status' => false, 'message' => 'Something went wrong. Please try again'], 422);
        }
    }

    public function delete(Request $request)
    {
        try {
            return DB::transaction(function () use ($request) {
                User::where('id', $request->id)->delete();
                return response()->json(['status' => true, 'message' => 'User deleted successfully', 'users' => User::all()]);
            });
        } catch (Throwable $e) {
            return response()->json(['status' => false, 'message' => 'Something went wrong. Please try again'], 200);
        }
    }
}

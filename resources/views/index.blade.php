<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Management System</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.7.18/sweetalert2.min.css"/>
    <style>
        .container {
            margin-top: 50px;
        }

        .user-list {
            margin-bottom: 30px;
        }

        .user-list th {
            font-weight: bold;
        }

        .user-list td, .user-list th {
            padding: 8px;
            border: 1px solid #ddd;
        }

        .user-list img {
            max-width: 100px;
        }

        .user-form {
            margin-top: 30px;
        }

        .user-form input[type="file"] {
            margin-top: 5px;
        }

        .preview-image {
            max-width: 200px;
            margin-top: 10px;
        }

        .btn-golden {
            background-color: #FFD700;
            color: #000;
            border-color: #FFD700;
        }
    </style>
</head>
<body class="antialiased">
<div id="app" class="container">
    <div class="user-list">
        <h2>User List</h2>
        <div class="form-inline mb-3">
            <input type="text" class="form-control mr-2" v-model="searchName" placeholder="Search by Name"  @keyup="filterUsers">
            <input type="text" class="form-control mr-2" v-model="searchEmail" placeholder="Search by Email"  @keyup="filterUsers">
            <select class="form-control" v-model="searchGender" @change="filterUsers">
                <option value="">All Genders</option>
                <option value="1">Male</option>
                <option value="2">Female</option>
            </select>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Gender</th>
                <th>Image</th>
                <th>File</th>
                <th>Address</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="(user, index) in users" :key="index">
                <td>@{{ user.name }}</td>
                <td>@{{ user.phone }}</td>
                <td>@{{ user.email }}</td>
                <td>@{{ user.gender === 1 ? 'Male' : 'Female' }}</td>
                <td>
                    <img v-if="user.image_path" :src="user.image_path" alt="User Image">
                    <span v-else>No Image</span>
                </td>
                <td>
                    <a v-if="user.file_path" :href="user.file_path" target="_blank">View File</a>
                    <span v-else>No File</span>
                </td>
                <td>@{{ user.address }}</td>
                <td>
                    <button type="button" class="btn btn-info btn-sm" @click="editUser(user.id)">
                        <i class="bx bx-edit-alt mr-1"></i>Edit
                    </button>
                    <button type="button" class="btn btn-danger btn-sm" @click="deleteUser(user.id)">
                        <i class="bx bx-trash mr-1"></i>Delete
                    </button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="user-form">
        <h2>User Form</h2>
        <form @submit.prevent="submitForm">
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" v-model="name" placeholder="Enter Name">
            </div>
            <div class="form-group">
                <label>Gender:</label><br>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="male" value="1" v-model="gender">
                    <label class="form-check-label" for="male">Male</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="female" value="2" v-model="gender">
                    <label class="form-check-label" for="female">Female</label>
                </div>
            </div>
            <div class="form-group">
                <label for="phone">Mobile:</label>
                <input type="text" class="form-control" name="phone" v-model="phone" placeholder="Enter Mobile">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" class="form-control" name="email" v-model="email" placeholder="Enter Email">
            </div>
            <div class="form-group">
                <label for="address">Address:</label>
                <input type="text" class="form-control" name="address" v-model="address" placeholder="Enter Address">
            </div>
            <div class="form-group">
                <label for="image">Image:</label>
                <input type="file" class="form-control-file" name="image" @change="onImageChange">
                <img v-if="imageUrl" :src="imageUrl" alt="Preview" class="preview-image">
            </div>
            <div class="form-group">
                <label for="file">File:</label>
                <input type="file" class="form-control-file" name="file" @change="onFileChange">
                <span v-if="fileName">@{{ fileName }}</span>
            </div>
            <button type="button" @click="onsubmit" class="btn btn-block btn-lg btn-golden">
                <i class="bx bx-plus mr-1"></i> @{{ btnValue }}
            </button>
        </form>
    </div>
</div>
</body>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.7.18/sweetalert2.all.js"></script>

<script>
    new Vue({
        el: '#app',
        data: {
            users: {!! json_encode($users) !!},
            allUser: {!! json_encode($users) !!},
            name: '',
            phone: '',
            address: '',
            gender: 1,
            email: '',
            userId: null,
            image: null,
            imageUrl: '',
            file: null,
            fileName: '',
            btnValue: 'Submit',
            searchName: '',
            searchEmail: '',
            searchGender: '',
        },
        methods: {
            filterUsers() {
                let filteredUsers = this.allUser;
                let check = true;
                if (this.searchName) {
                    this.users = filteredUsers.filter(user => user.name.toLowerCase().includes(this.searchName.toLowerCase()));
                    check = false;
                }

                // Filter by email
                if (this.searchEmail) {
                    this.users = filteredUsers.filter(user => user.email.toLowerCase().includes(this.searchEmail.toLowerCase()));
                    check = false;
                }

                // Filter by gender
                if (this.searchGender) {
                    this.users = filteredUsers.filter(user => user.gender.toString() === this.searchGender);
                    check = false;
                }
                if (check) {
                    // Update the filteredUsers array
                    this.users = filteredUsers;
                }
            },
            updateGender(value) {
                this.gender = value;
            },
            onImageChange(event) {
                this.image = event.target.files[0];
                this.imageUrl = URL.createObjectURL(this.image);
            },

            onFileChange(event) {
                this.file = event.target.files[0];
                this.fileName = this.file.name;
            },
            onsubmit() {
                axios.post('store', {
                    name: this.name,
                    phone: this.phone,
                    address: this.address,
                    email: this.email,
                    gender: this.gender,
                    image: this.image,
                    file: this.file,
                    id: this.userId,
                }, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    if (response.data.status === true) {
                        this.users = response.data.users;
                        Swal.fire('Yay!!!', response.data.message, 'success');
                        this.emptyForm()
                    } else {
                        Swal.fire('Oops!!!', response.data.message, 'error');
                    }
                }).catch(error => {
                    if (error.response.status === 422) { // Validation error
                        let errors = error.response.data.errors;
                        for (let key in errors) {
                            if (errors.hasOwnProperty(key)) {
                                Swal.fire('Oops!!!', errors[key][0], 'error');
                            }
                        }
                    } else {
                        console.error('Error:', error.response);
                        Swal.fire('Oops!!!', 'An error occurred while saving the user.', 'error');
                    }
                });
            },
            editUser(id) {
                this.btnValue = 'Update';
                this.userId = id;
                const user = this.users.find(user => user.id === id);
                if (user) {
                    this.name = user.name;
                    this.phone = user.phone;
                    this.email = user.email;
                    this.address = user.address;
                    this.gender = user.gender;
                }
            },
            emptyForm() {
                this.name = '';
                this.phone = '';
                this.email = '';
                this.address = '';
                this.gender = 1;
                this.image = null;
                this.imageUrl = null;
                this.file = null;
                this.fileName = null;
                this.btnValue = 'Submit';
            },
            deleteUser(id) {
                axios.get('user-delete', {
                    params: {
                        id: id
                    }
                }).then(response => {
                    if (response.data.status === true) {
                        this.users = response.data.users;
                        Swal.fire('Yay!!!', response.data.message, 'success');
                    } else {
                        Swal.fire('Oops!!!', response.data.message, 'error');
                    }
                }).catch(error => {
                    console.error('Error:', error.response);
                });
                this.emptyForm()
            },
        }
    })
</script>
</html>

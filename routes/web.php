<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'UserController@index')->name('index');
Route::post('store', 'UserController@store')->name('store');
Route::get('user-delete', 'UserController@delete')->name('user-delete');
